function enviar(){

    var data = {
        "email": document.getElementById("inputEmail").value,
        "name": document.getElementById("inputNome").value,
        "lastname": document.getElementById("inputSobrenome").value,
        "password": document.getElementById("inputPassword").value,
        "dof": document.getElementById("dof").value
    }

    post('http://localhost:3000/customers', data); // Faz um post para cadastrar um usuário

}

function post(url, data)
{
    var json = JSON.stringify(data);
    //alert(json);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhr.onload = function () {
        var customer = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "201") {
            console.error(customer);
        } else {
            alert("Erro ao cadastrar usuário!");
        }
    }

    xhr.send(json);
}