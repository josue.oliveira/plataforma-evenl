'use strict'

const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    /*res.status(200).send({
        title: "EvenL RESTful API",
        version: "1.0.0"
    });*/

    res.sendFile("home.html", {"root": "E:\\EvenL\\API\\public"});
});

module.exports = router;